# のろし沼ってなんぞや

知らん。6/4にDiscordにDDosが発生していた時に突如としてまってぃーというTwitter垢に知り合いからなんらかの不具合が起きた際に送られたという画像とともに、そのメッセージにはのろし沼と書かれていたという情報が出された

# twitter

## MQtty / まってぃ@9xUxmfop3hSwcYv

### 2023/6/4 PM10:39

```
この画像を知っている人たちはいますか？
Discordがバグって友達から勝手に送られてきた画像なのですが知っている人いたら教えてください！

#discord #拡散希望 #怖い

```

添付画像

<img src="image/まとめ/1686140182454.png" height = "500"><img src ="image/まとめ/1686140585207.png" height = "500">

#### リプライにて

```
追伸です
これらの画像が送られた際、「のろし沼」というメッセージと共に送られてきました。関係性がわかる方は教えてください。
```

### 2023/6/4 PM11:14

```
Discordいまddos攻撃受けてるらしいですね…
早く治るといいな
#Discord
```

### 2023/6/5 PM5:33

```
自己紹介です
どうもまってぃーです！
中学生でイラストとか描いたりしてまーす！
受験生なので不定期ですが応援していただけると嬉しいです😃

#自己紹介
#中学生
#イラスト好きさんとつながりたい 
#イラスト初心者
#Twitterはじめました
```

<img src="image/まとめ/1686141282489.png" height = "500">

## C´@ceremony69642

### 2023/6/4 PM11:42

```
この画像が急にdiscordで送られてきたのですが、知っている方はいますか。怖かったので緊急で聞いてます。のろし沼、と言ってました。#discord
```

## 81HkMQiFHN @sNLFi6bINI

### 2023/6/5 AM2:49

<img src="image/まとめ/1686141815593.png" height = "500">

### 2023/6/5 AM2:54

[添付動画](Video/sNLFi6bINI-1665416722718027776-2176kbps.mp4)

新しいタブで開くことを推奨します

### 2023/6/5 PM7:56

<img src="image/まとめ/1686194459794.png" height = "500">

### 2023/6/5 PM7:57

<img src="image/まとめ/1686194847798.png" height = "500">

### 2023/6/6 PM7:22

```
fromのろし沼
```

添付画像

<img src="image/まとめ/1686194984935.png" height = "500">

### 2023/6/6 PM8:14

```
Coordinate20:14
```

<img src="image/まとめ/1686195296445.png" height = "500">

### 2023/6/7 AM1:34

```
誰かマンホールの開け方ってわかる人いますか？？のろし沼に入りたいんです
```

### 2023/6/7 AM5:32

```
バール使ったら簡単にマンホール開いたよ
まだのろし沼入らないけど入ったら帰ってこれるか分からないね
```

添付画像

<img src="image/まとめ/1686195695016.png" height = "500" ><img src="image/まとめ/1686195833007.png" height = "500">

### 2023/6/7 PM10:02

```
のろし沼に入ります
```

添付画像

<img src="image/まとめ/1686199475757.png" height = "500">

引リツ

[引リツ元](#202367-am532)

#### リプライにて

```
ﾅｷﾞｨ=ｻｰﾝ@naoneko_cat
   動画取れますか？ 
   取れたら欲しいです。


81HkMQiFHN@sNLFi6bINI
   動画だと何かあった時に、すぐ送信することができないので、無理です


ﾅｷﾞｨ=ｻｰﾝ@naoneko_cat
   わかりました。
   何かあった時のため、なるべく高頻度で更新をお願いしますね。少し心配なので。


81HkMQiFHN@sNLFi6bINI
   はい。
```

### 2023/6/7 PM10:05

```
今入っています
のろし沼
22:05
```

添付画像

<img src="image/まとめ/1686201962115.png" height = "500">

### 2023/6/7 PM10:08

```
暗い
22:08
```

添付画像

<img src="image/まとめ/1686207274167.png" height = "500">

### 2023/6/7 PM10:09

```
部屋の一角です
22:09
```

添付画像

<img src="image/まとめ/1686209019998.png" height = "500">

### 2023/6/7 PM10:10

```
22:10
```

添付画像

<img src="image/まとめ/1686209107990.png" height = "500">

### 2023/6/7 PM10:11

```
22:11
```

添付画像

<img src="image/まとめ/1686209182101.png" height = "500">

### 2023/6/7 PM10:12

```
22:12
```

添付画像

<img src="image/まとめ/1686209242254.png" height = "500">

### 2023/6/7 PM10:13

```
22:13
```

#### 注釈

投稿された瞬間には同日午後22:12分に投稿されていた写真から、さらに出口と思われる場所に近づいた写真が添付されていたが、しばらく後にその添付画像が削除された

意図は不明

### 2023/6/7 PM10:14

```
22:14
```

### 2023/6/7 PM10:17

<img src="image/まとめ/1686209664853.png" height = "500">

### 2023/6/7 PM10:18

同時刻に３枚写真が投稿されたため連続で紹介する

それぞれ画像のみ

<img src="image/まとめ/1686209732191.png" height = "500"><img src="image/まとめ/1686212499851.png" height = "500"><img src="image/まとめ/1686212792758.png" height = "500">

### 2023/6/7 PM10:19

```
待ってのろし沼
```

### 2023/6/7 PM10:23

```
22
```

### 2023/6/8 PM8:33

```
色々あってそっちに戻れそうにないのでのろし沼の情報だけ伝えます。
座標:>j^4ty|5da!:rc'k5]05H1
[RXe%?Ds;[Lv\e"|'rVR7R
AfIX*Q_OJTn)u\M4J8P+L:
hS}|NxtP"oqDKuSAW#;d-<
l)b>RX&bbBB>qgk3TP,jHv
GAr{C-:OSIV[?'EL7rqj4I
```

### 2023/6/8 PM8:37

```
ドアを開けて階段を降ったらロッカーのような部屋があります。奥には行かないでください。道中にマンホールがあります。その中がのろし沼です。座標は意味不明な文字列ではありません。追跡者には分かるはずです。それではさようなら、
```

引用リツイート

[引リツ元(一つ前のツイート)](#202368-pm833)

### 2023/6/12

```
お久しぶりです
追跡者がなかなかやってこないので、もう少し情報を。のろし沼は横浜市です。
X 92372431 Z45322626
```

## 異端観測@itan_kansoku

### 2022/9/5 AM7:42

```
108回目の遭遇
```

添付画像

<img src="image/まとめ/1686213223299.png" height = "500">

### 2022/9/6 PM3:17

```
友達
```

添付画像

<img src="image/まとめ/1686213378347.png" height = "500">

### 2022/10/5 AM7:53

```
人を探しています
```

### 2022/10/6 AM9:34

```
ヒルガミサマ
```

添付画像

<img src="image/まとめ/1686213580434.png" height = "500">

### 2022/10/6 AM9:34(2)

```
ヒルガミサマを観測してきました
約束はまだ守られているようです
```

### 2022/10/11 AM5:25

```
人を探しています
```

### 2023/1/29 PM1:54

```
ひとりいなくなりました
```

### 2023/6/4 AM11:17

```
人を探しています。
```

### 2023/6/4 PM0:03

```
ながいです
```

添付画像

<img src="image/まとめ/1686213947186.png" height = "500">

### 2023/6/5 PM3:43

```
のろし沼は
異端観測地ではないはずです
人を探していますが
いません
人を探しています
```

#### リプライにて

```
MQtty / まってぃー@9xUxmfop3hSwcYv
   こんにちは
   質問ですがのろし沼ってなんですか？
   それと誰を探しているのですか？
   教えてください。


異端観測@itan_kansoku
   のろし沼は実在する非実在空間だとおもって下さい　人がつたえるロアは事実となることがある　今回はそうなるでしょうか

   私が探しているのは　あるこどもです

   性別となまえはわかりません
   探しています　彼もしくは彼女は私の観測によって同定されるのでしょうか
   おねがいします
   人を探しています
```

### 2023/6/5 AM3:25

```
ろあ　とはひとが口に出すことで
ちからをもつ

それが真実ではなくても
事実と　なり得る
それがしんじつであっても
事実ではなくなる
ロア　とはひとが口に出すことで
loreとなる。

ひとはそれを可能とする。

人を探しています。325。
```

### 2023/6/7 AM4:06

```
406
```

### 2023/6/7 AM4:13

```
ろあが命を持ち始めています
以前このくにではないばしょで
ろあが産み落とされ
けっか　人はそれをとある部屋の名前でよびました

異端観測に場所は関係ありません
これから180日のあいだ
ろあの行く末をみまもりましょう

人を探しています。おねがいします。探しています。おねがいします。
```

### 2023/6/7 AM4:17

```
わたしの知る
行く道です　あの頃私はまだ探してはいませんでしたが
ろあはどこからでも繋がることができます

おじぎはたいせつです
```

添付画像

<img src="image/まとめ/1686214610265.png" height="500">

### 2023/6/8 AM0:57

```
あなたが先ほど観測していた　彼もしくは彼女を
私も観測しました

今回のろあは生まれたてのとても小さなものですが　あなたが大切に育てることで
ろあとしての価値をてにい　れるのです
作られたものが命を手に入れる　その助けをあなたがする　たいせつなおしごとですおしごとはたいせつです
```

### 2023/6/8 AM1:12

```
かつて　とある蓋が　知られることがありました　あれは質の良いロアでした
が　最後に暴かれました

あなたはこれを　どうおもいますか

命を宿したロあはしぬでしょうか。
```

添付画像

<img src="image/まとめ/1686214798082.png" height="500">

### 2023/6/8 AM1:45

```
彼もしくは彼女も　地下構造物の観測を行なっていたので
わたしも観測にきました

最近たずねた　観測地では
いちばんあぶないです

幸運なことに　私は恐らく大多数の人がきが付かずにいるものを見るだけです　から
ふかみまで　たちいるつもりはありません
ここも外から観測するに留めておきます
145
```

### 2023/6/9 AM0:50

```
彼もしくは彼女は　ロアの終着点を定めたようですね
ここからは　ひとが　どのようにそれを扱うのか　ろあが命を持つかは　人しだいです
わたしはただ　よりよいけっかを望むだけです
```

### 2023/6/9 AM0:58

```
私を観測しているあなた方へ

本来わたしは観測されることがなかったはずの　とても不確かな存在でした

しかし今回のことで目を向けられました　私は人を探しています

ワタシが探すしているのはこどもですがあなたがたがそれを知るかは分かりません　がなにか助けになるかもしれません
```

### 2023/6/9 AM1:00

```
挨拶もかねてひとつ　わたしを観測しているあなたにヒントです

観測して　認識する

一見逆におもえるでしょう

しかし　たしかに観測して認識するのです

認識の前に観測という行為をし　結果あなたはそれらを認識する　いちど　慣れてしまえば　簡単なはずです
```

### 2023/6/9 AM1:10

```
そも　あなたはそして彼彼女らは　じしんが幼少の頃に　異端観測をしれずに行っていたでしょう　少なくともひとの半数はですが

記憶はとても大切な花　記憶を現実になぞり観測し認識する　同定する
あなたは　ふたたび異端を観測するすべをもっているのです

われわれはあなたを歓迎します
```

### 2023/6/10 PM1:37

```
人を探しています。
```

## アライグマ@6kIpCUAxtOfk7UV(私)

### 2023/6/8 PM10:26

```
有識者助けてクレメンス
```

引用リツイート

[引リツ元](#202368-pm833)

#### リプライにて

```
C´@ceremony69642
   私には分かりました…この座標はおそらくこの場所のことで実家のすごい近くなんです…警察に言おうか自分で行こうか…親に何かあったら怖いので…


ﾅｷﾞｨ=ｻｰﾝ@naoneko_cat
   もし可能なら、警察に言っていいと思います。


C´@ceremony69642
   普通に怖いので言おうかと…
   家族が心配なので明日実家に帰ってみます


ﾅｷﾞｨ=ｻｰﾝ@naoneko_cat
   念の為警察を呼んで一緒に行った方がいいかもです。
   もし可能なら状況も1時間~30分くらいを目安に教えていただけるとこちらからも協力しやすいので、どうにかお願いします。


C´@ceremony69642
   分かりました…警察を呼ぶかは考えておきます…
```

```
C´@ceremony69642
   私には分かりました…この座標はおそらくこの場所のことで実家のすごい近くなんです…警察に言おうか自分で行こうか…親に何かあったら怖いので…


アライグマ@6kIpCUAxtOfk7UV
   なるほど、、これは心配ですね、、
   どうぞお気をつけてください、、
   ちなみに具体的な建物とかには心当たりがありますか？


C´@ceremony69642
   ありがとうございます…
   全くありませんね…
```

```
C´@ceremony69642
   私には分かりました…この座標はおそらくこの場所のことで実家のすごい近くなんです…警察に言おうか自分で行こうか…親に何かあったら怖いので…


MQtty / まってぃー@9xUxmfop3hSwcYv
　　都道府県だけでも教えていただけますでしょうか

　　自分の近くにあったら非常に怖いので教えていただきたいです。


C´@ceremony69642
　　関東です…補足ですがこの暗号は多分私の住所付近だから、私が気づけたのが大きいと思うので、無理に解読するのは難しいと思います…



MQtty / まってぃー@9xUxmfop3hSwcYv
　　関東ですか…

　　情報提供ありがとうございます
```

# 考察

1. 異端観測は今登場している人物の中でも人物を探すことを目的としている
2. 81...とC'は同じ場所に辿り着いた？
3. まってぃーとC'は81...からのろし沼の画像を送られている可能性あり（少なくともC'）

   1. 81...の6/5 PM7:56の画像がC'の6/4 PM11:42
4. 私自身(アライグマ)意味はないと思いつつのろし沼の座標ツイートを引用リツイートしたところC'が反応した

   1. 物語(?)上で詰まった時などには物語上の登場人物に何らかのアクションを取ることで解決もしくは物語が進むかもしれない
   2. 前提としてDiscord鯖での座標の解読に詰まっていた
5. 登場人物達と我々観測している側は違う次元（並行世界）のようなところにいるのではないか

   1. 我々には解読できなかった座標がC'は解読できていた
6. 108回目の遭遇について
7. 異端とされてる地域(?)とはLiminal Spaceのような場所では？

   1. Liminal Spaceとは簡単に言うと日常生活の中で、不気味だな、なんとなく嫌だな、と感じる空間の事
      知る人ぞ知るBackroomsの入り口の役目も果たしているそう
8. ヒルガミサマはのろし沼と関わりがある？

   1. 異端観測の地にまつわる何かしらなのか？
9. 異端観測が時々文末につける数字について

   1. 時刻を表しているっぽい
   2. 何故わざわざ時刻を表す必要が？
   3. ソーラーコード、ルナコードなどに関係があるのか？
10. 22:14分以降に投稿された画像は入る前の画像？

    1. 81HkMQiFHNがのろし沼から出れなくなったとすると
       のろし沼の情報を伝えようとした、、？
11. 異端観測⇒追跡者
    81⇒探されている子供?
12. のろしぬま⇒観測地のひとつ？

    1. 本来ならそうではなかったが、イレギュラーが発生している？
13. ロア （Loa） またはロワ （Lwa, Lwha） とは、ヴードゥー教に伝わる精霊の総称。一応アフリカ起源の 神々を中心とした体系を持つ

    表面上カトリックと自称するヴードゥーにおいて、ロアはいわゆる神Godと区別され、Aftergodと呼称され「守護聖人」で表され現世利益を求めるために崇拝される。
14. 異端観測関係ない？

# 資料

## のろし沼の入り口(?)見取り図

<img src="image/まとめ/1686570970852.png" height="500"><img src="image/まとめ/1686571194283.png" height="500">
